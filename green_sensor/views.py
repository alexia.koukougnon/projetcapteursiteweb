# Toutes les vues en commentaire ne sont pas faites et sont à faire.

from datetime import datetime
from .app import app, db
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField
from flask import render_template, url_for, redirect, request
from .models import *
from wtforms import PasswordField
from hashlib import sha256
from flask_login import login_user, current_user, logout_user, login_required
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None


@app.route("/", methods=("GET",))
def accueil():
    return render_template(
        "accueil.html",
        title = "Green Sensor",
        titreCarte1 = "Bienvenue sur Green Sensor !",
        date = datetime.now().strftime("%d/%m/%y"),
        titreCarte2= "Utilisation d'une technologie Arduino",
        formuleGratuite=["Gratuite","Avec pub","5 capteurs disponibles","Acccès au site web"],
        formuleStandard=["28.60€/mois","Sans pub","15 capteurs disponibles","Installation","Maintenance","Localisation disponible"],
        formuleProfessionelle=["70.90€/mois","Sans pub","50 capteurs disponibles","Installation","Maintenance","Localisation disponible","+ d'option d'analyse"]
    )

@app.route("/Connexion", methods=("GET",))
def connexion_get():
    return render_template(
        "connexion.html",
        title = "Se connecter"
        )

# @app.route("/Connexion", methods=("POST",))
# def connexion_post():
#     return "pas encore implémenté", 501

@app.route("/Inscription", methods=("GET",))
def inscription_get():
    return render_template(
        "inscription.html",
        title = "Se créer un compte"
        )

# @app.route("/Inscription", methods=("POST",))
# def inscription_post():
#     return "pas encore implémenté", 501

# @app.route("/Motdepasseoublie", methods=("GET",))
# def mdp_get():
#     return "pas encore implémenté", 501

# @app.route("/Motdepasseoublie", methods=("POST",))
# def mdp_post():
#     return "pas encore implémenté", 501

# @app.route("/Moncompte/<int:id_personne>", methods=("GET",))
# def moncompte(id_personne):
#     return "pas encore implémenté", 501

@app.route("/Abonnement", methods=("GET",))
def abonnement_test():
    return render_template(
        "abonnement.html",
        title = "Abonnement",
        typeAbonnement = "Formule standard",
        prix = "28.60€/mois",
        caracteristique = ["Sans pub","15 capteurs disponibles","Installation","Maintenance","Localisation disponible"]
    )

@app.route("/Abonnement/<int:id_abonnement>", methods=("GET",))
def abonnement(id_abonnement):
    if (id_abonnement == 1):
        return render_template(
            "souscrireAbonnement.html",
            title = "Abonnement",
            typeAbonnement = "Formule gratuite",
            prix = "Gratuite",
            caracteristique = ["Avec pub","5 capteurs disponibles","Acccès au site web"]
    )   
    elif (id_abonnement == 2):
        return render_template(
            "souscrireAbonnement.html",
            title = "Abonnement",
            typeAbonnement = "Formule standard",
            prix = "28.60€/mois",
            caracteristique = ["Sans pub","15 capteurs disponibles","Installation","Maintenance","Localisation disponible"]
    )
    else :
        return render_template(
            "souscrireAbonnement.html",
            title = "Abonnement",
            typeAbonnement = "Formule professionelle",
            prix = "70.90€/mois",
            caracteristique = ["Sans pub","50 capteurs disponibles","Installation","Maintenance","Localisation disponible","+ d'option d'analyse"]
    )   


# @app.route("/Abonnement/Paiement/<int:id_abonnement>/<string:moyenpaiement>/", methods=("GET",))
# def paiement_abonnement_get(id_abonnement, moyenpaiement):
#     return "pas encore implémenté", 501

# @app.route("/Abonnement/Paiement/<int:id_abonnement>/<string:moyenpaiement>/", methods=("POST",))
# def paiement_abonnement_post(id_abonnement, moyenpaiement):
#     return "pas encore implémenté", 501

@app.route("/Contact", methods=("GET",))
def contact_get():
    return render_template(
        "contact.html",
        title = "Contact"
    )

# @app.route("/Contact", methods=("POST",))
# def contact_post():
#     return "pas encore implémenté", 501

@app.route("/Mesgroupe", methods=("GET",))
def mesgroupes_post():
    return render_template(
        "groupe.html",
        title = "Mes groupes",
        nomGroupe = "Serre A",
        Description = "test",
        Administrateur = "Damien Rian"
    )
# @app.route("/Mesgroupe/<int:id_groupe>", methods=("GET",))
# def mesgroupes(id_groupe):
#     return "pas encore implémenté", 501

# @app.route("/Mesgroupe/modifier/<int:id_groupe>", methods=("GET",))
# def mesgroupes_modifier_get(id_groupe):
#     return "pas encore implémenté", 501

# @app.route("/Mesgroupe/modifier/<int:id_groupe>", methods=("POST",))
# def mesgroupes_modifier_post(id_groupe):
#     return "pas encore implémenté", 501

@app.route("/Mesgroupe/ajout", methods=("GET",))
def ajouter_groupe_get():
    return render_template(
        "creationGroupe.html",
        title = "Créer votre groupe"
    )

# @app.route("/Mesgroupe/ajouter", methods=("POST",))
# def ajouter_groupe_post():
#     return "pas encore implémenté", 501

# @app.route("/Mesgroupe/supprimer/<int:id_groupe>", methods=("GET",))
# def supprimer_groupe_get(id_groupe):
#     return "pas encore implémenté", 501

# @app.route("/Mesgroupe/supprimer/<int:id_groupe>", methods=("POST",))
# def supprimer_groupe_post(id_groupe):
#     return "pas encore implémenté", 501

# @app.route("/OutilsAnalyse/<int:id_capteur>", methods=("GET",))
# def outilsAnalyseCapteur(id_capteur):
#     return "pas encore implémenté", 501

@app.route("/OutilsAnalyse", methods=("GET",))
def outilsAnalyse():
    return render_template(
        "outilAnalyse.html",
        title = "Outil D'analyse"
    )


# @app.route("/Mesgroupe/ajoutcapteur/<int:id_groupe>", methods=("GET",))
# def mesgroupes_ajoutcapteur_get(id_groupe):
#     return "pas encore implémenté", 501

# @app.route("/Mesgroupe/ajoutcapteur/<int:id_groupe>", methods=("POST",))
# def mesgroupes_ajoutcapteur_post(id_groupe):
#     return "pas encore implémenté", 501

# @app.route("/Mesgroupe/<int:id_groupe>/<int:id_capteur>", methods=("GET",))
# def mesgroupes_capteur(id_groupe, id_capteur):
#     return "pas encore implémenté", 501

@app.route("/detailsCapteur",methods=("GET",) )
def detailsCapteur_parGroupe():
    return render_template(
        "detailsCapteur.html",
        title = "Groupe Serre B",
        title2 = "Capteur présente sur ma carte :"
    )

@app.route("/Motdepasseoublie", methods = ("GET","POST"))
def mdp_get():
    return render_template(
        "resetPassword.html",
        title = "Mot de passe oublié ?"
    )

@app.route("/souscrireAbonnement", methods = ("GET",))
def souscrireAbonnement():
    return render_template(
        "souscrireAbonnement.html",
        title = "Souscrire à un abonnement",
        typeAbonnement = "Formule standard",
        prix = "28.60€/mois",
        caracteristique = ["Sans pub","15 capteurs disponibles","Installation","Maintenance","Localisation disponible"]
    )

@app.route("/Paiement", methods=("GET",))
def paiement_abonnement():
    return render_template(
        "paiement.html",
        title="Souscrire à un abonnement",
        typeAbonnement = "Formule standard"
    )

@app.route("/AjoutCapteur", methods=("GET",))
def ajoutCapteur():
    return render_template(
        "ajoutCapteur.html",
        title="Ajouter un capteur"
    )

@app.route("/FicheIdentiteCapteur", methods=("GET",))
def ficheIdentiteCapteurGroupe():
    return render_template(
        "ficheIdentiteCapteur.html",
        title="Fiche identité",
        NomCapteur="M97658IO",
        TypeCapteur = "Taux d'humidité",
        Localisation = "IUT informatique Orléans",
        DateConsultation= datetime.now().strftime("%d/%m/%y"),
        Caractéristique = ["Ce capteur est composé d’un module SIM 808 afin d’assurer la localisation GPS ",
        "Dimension : 70 x 54 x 15 ",
        "équipée d'un microcontrôleur ATmega32u4 "],
        Interfaces = ["20 broches d’E/S dont 6 PWM",
        "6 entrées analogiques 10 bits",
        "bus série, I2C et SPI"]
    )

@app.route("/CharteGraphique", methods=("GET",))
def charteGraphique():
    return render_template(
        "charteGraphique.html",
        title="Charte Graphique"
    )

@app.route("/PresentationGreenSensor", methods=("GET",))
def presentation():
    return render_template(
        "presentation.html",
        title="Présentation de Green Sensor"
    )