# Green Sensor
Bienvenue sur le GIT du projet capteur !

## Pour commencer

Premièrement il faut installer son environnement virtuel dans le dossier __ProjetCapteurSiteWeb__.
Pour cela il faut donc utiliser la commande suivante : 

```bash
virtualenv -p python3 venv
```

Puis pour activer l'environnement virtuel, il faut utiliser cette commande :

```bash
source venv/bin/activate
```

Enfin il faut installer tout ce qu'il faut pour que ça marche avec cette commande :

```bash
pip install -r requirements.txt
```

Pour lancer le projet il faut utiliser cette commande :

```bash
flask run
```

## Manuel utilisateur

Si vous voulez plus d'information sur comment utiliser notre site internet, vous retrouverez tout [ici](https://docs.google.com/document/d/1pSXreVIakQb8gsmlGWiL0S0tbLNXaXtOXqvxItCbEF4/edit?usp=sharing) !

## Autre

Voici le lien sur notre autre GIT [ici](https://gitlab.com/Tidam/projetcapteur2a) !


