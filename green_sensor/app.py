from flask import Flask 
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

import os.path
def mkpath (p):
    return os.path. normpath (
        os.path.join(
            os.path. dirname ( __file__ ),p
        )
    )



app = Flask(__name__)
Bootstrap(app)
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../myapp.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = "8155b376-be59-4837-abd0-0432ce48feb6"
db = SQLAlchemy(app)

login_manager = LoginManager(app)
login_manager.login_view = "login"